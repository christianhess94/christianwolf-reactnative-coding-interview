import React from 'react';

import { createStackNavigator } from '@react-navigation/stack';
// import { createDrawerNavigator } from '@react-navigation/drawer';
import { EmployeeDetailScreen, EmployeesScreen } from '../screens';
import { ScreenParamsList } from './paramsList';

const Stack = createStackNavigator<ScreenParamsList>();
export const headerTintColor = '#6C3ECD';
export function MainNavigator() {
  // const Drawer = createDrawerNavigator<ScreenParamsList>();
  return (
    <Stack.Navigator
      initialRouteName="Employees"
      screenOptions={{
        headerTintColor: '#6C3ECD',
        headerTitleAlign: 'center',
      }}>
      <Stack.Screen component={EmployeesScreen} name="Employees" />
      <Stack.Screen component={EmployeeDetailScreen} name="EmployeeDetail" />
    </Stack.Navigator>
  );
}
