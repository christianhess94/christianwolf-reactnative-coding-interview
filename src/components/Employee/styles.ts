import { StyleSheet } from 'react-native';
import { headerTintColor } from '../../navigators';

export default StyleSheet.create({
  employeeItemCard: {
    flex: 1,
    backgroundColor: headerTintColor,
    margin: 5,
    borderRadius: 4,
    flexDirection: 'row',
    padding: 10,
    paddingVertical: 15,
  },
  employeeItemList: {
    margin: 5,
    borderRadius: 4,
    flexDirection: 'row',
    padding: 10,
    paddingVertical: 15,
  },
  employeeAvatar: {
    height: 48,
    width: 48,
    borderWidth: 1,
    borderRadius: 100,
    marginHorizontal: 10,
  },
  employeeInfo: {
    justifyContent: 'space-around',
  },
});
